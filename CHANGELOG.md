# Next

* [Remove parent from query constructor](https://gitlab.com/Vlad160/odata-query-builder/issues/5)
* [Cleanup binary and binary expressions](https://gitlab.com/Vlad160/odata-query-builder/issues/6)
* [Add possibility to append raw query](https://gitlab.com/Vlad160/odata-query-builder/issues/4)
* [Support $levels](https://gitlab.com/Vlad160/odata-query-builder/issues/9)
* [Support nested filters](https://gitlab.com/Vlad160/odata-query-builder/issues/2)

# 1.1.1 - 23/11/2018

* Updated Readme

# 1.1.0
* Empty filter query bigfix
* Count operator support
* Integrated tests