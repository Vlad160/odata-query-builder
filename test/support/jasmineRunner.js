const path = require('path');
const { register } = require('ts-node/dist');
const { argv } = require('yargs');

const TS_NODE_OPTIONS = [
    'fast',
    'lazy',
    'cache',
    'cacheDirectory',
    'compiler',
    'project',
    'ignore',
    'ignoreWarnings',
    'disableWarnings',
    'getFile',
    'fileExists',
    'compilerOptions',
];

const tsNodeOptions = Object.assign({}, ...TS_NODE_OPTIONS.map((option) => argv[option] && { [option]: argv[option] }));
register(tsNodeOptions);

const Jasmine = require('jasmine');
const Command = require('jasmine/lib/command');

const jasmine = new Jasmine({ projectBaseDir: path.resolve() });
const examplesDir = path.join('node_modules', 'jasmine-core', 'lib', 'jasmine-core', 'example', 'node_example');
const command = new Command(path.resolve(), examplesDir, console.log);


const jasmine_args = process.argv.slice(2).reduce((acc, k) => {
    if (k.indexOf('--') > -1) {
        if (!TS_NODE_OPTIONS.includes(k.replace('--', ''))) {
            acc.push(k)
        }
    }
    return acc;
}, []);

command.run(jasmine, jasmine_args);