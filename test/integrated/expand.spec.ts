import { OData4Query } from '../../src/odata';
import { buildNorthWindUrl, doFetch, buildTripPinUrl } from './utils';
import { CUSTOMERS, PEOPLE } from './constants';

describe('Expand specs', () => {

    let odataQuery: OData4Query;

    beforeEach(() => {
        odataQuery = new OData4Query();
    });
    it('Expand without nested query', async () => {
        const query = odataQuery.expand('Orders').toString({ encode: true });
        const url = buildNorthWindUrl(CUSTOMERS, query);
        const json = await doFetch(url);
        const expected = json.value.every(x => typeof x.Orders === 'object');
        expect(expected).toBe(true);
    });
    it('Expand with query', async () => {
        const query = odataQuery
            .expand('Trips', q =>
                q.filter(f => f
                    .eq('Name', 'Trip in US')))
            .toString({ encode: true });
        const url = buildTripPinUrl(PEOPLE, query);
        const json = await doFetch(url);
        const expected = json.value.every(x => typeof x.Trips === 'object' && x.Trips.every(t => t.Name === 'Trip in US'));
        expect(expected).toBe(true);
    })
});
