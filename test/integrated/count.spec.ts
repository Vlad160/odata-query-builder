import { OData4Query } from '../../src/odata';
import { doFetch, buildTripPinUrl } from './utils';
import { PEOPLE } from './constants';

describe('Count specs', () => {

    let odataQuery: OData4Query;

    beforeEach(() => {
        odataQuery = new OData4Query();
    });
    it('Count', async () => {
        const query = odataQuery
            .count()
            .toString({ encode: true });
        const url = buildTripPinUrl(PEOPLE, query);
        const json = await doFetch(url);
        const expected = typeof json['@odata.count'] === 'number';
        expect(expected).toBeTruthy();
    })
});
