import { OData4Query, OData4Filter } from '../../src/odata/odata4';
import { CUSTOMERS } from './constants';
import { doFetch, buildNorthWindUrl } from './utils';
import { LogicalOperator } from '../../src/filter/ODataFilter';
import { eq } from '../../src/operators/filter';

describe('Filter integrated tests', () => {

    let odataFilter: OData4Query;

    beforeEach(() => {
        odataFilter = new OData4Query();
    });

    it('city should be Berlin', async () => {
        const filter = odataFilter.filter(f => f.eq('City', 'Berlin')).toString({ encode: true });
        const url = buildNorthWindUrl(CUSTOMERS, filter);
        const json = await doFetch(url);
        const expected = json.value.every(x => x.City === 'Berlin');
        expect(expected).toBe(true);
    });

    it('city should not be Berlin', async () => {
        const filter = odataFilter.filter(f =>
            f.not(x => x.eq('City', 'Berlin'))).toString({ encode: true });
        const url = buildNorthWindUrl(CUSTOMERS, filter);
        const json = await doFetch(url);
        const expected = json.value.every(x => x.City !== 'Berlin');
        expect(expected).toBe(true);
    });

    it('City to upper eq London', async () => {
        const filter = odataFilter.filter(f => f.eq(x => x.toUpper('City'), 'London')).toString({ encode: true });
        const url = buildNorthWindUrl(CUSTOMERS, filter);
        const json = await doFetch(url);
        const expected = json.value.every(x => x.City === 'London');
        expect(expected).toBe(true);
    });

    it('country UK or Germany', async () => {
        const filter = new OData4Filter(LogicalOperator.OR)
            .pipe(
                eq('Country', 'Germany'),
                eq('Country', 'UK'))
            .toString({ encode: true });
        const url = buildNorthWindUrl(CUSTOMERS, `$filter=${filter}`);
        const json = await doFetch(url);
        const expected = json.value.every(x => x.Country === 'Germany' || x.Country === 'UK');
        expect(expected).toBe(true);
    })
});
