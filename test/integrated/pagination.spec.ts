import { OData4Query } from '../../src/odata';
import { buildNorthWindUrl, doFetch } from './utils';
import { CUSTOMERS } from './constants';

describe('Pagination tests', () => {
    let odataQuery: OData4Query;

    beforeEach(() => {
        odataQuery = new OData4Query();
    });

    it('Top', async () => {
        const query = odataQuery
            .top(2)
            .toString({ encode: true });
        const url = buildNorthWindUrl(CUSTOMERS, query);
        const response = await doFetch(url);
        const expected = response.value.length <= 2;
        expect(expected).toBeTruthy();
    });

    it('Skip', async () => {
        const query = odataQuery
            .toString({ encode: true });
        const url = buildNorthWindUrl(CUSTOMERS, query);
        const response = await doFetch(url);
        const firstValue = response.value[0];
        const skipQuery = new OData4Query()
            .skip(1)
            .toString({ encode: true });
        const skipUrl = buildNorthWindUrl(CUSTOMERS, skipQuery);
        const skipResponse = await doFetch(skipUrl);
        expect(skipResponse.value[0]).not.toEqual(firstValue);
    })
});
