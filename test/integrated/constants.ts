export const NORTWIND_V4_BASE = 'https://services.odata.org/V4/Northwind/Northwind.svc';
export const TRIP_PIN_BASE = 'https://services.odata.org/V4/TripPinServiceRW';
export const CUSTOMERS = 'Customers';
export const CATEGORIES = 'Categories';
export const EMPLOYEES = 'Employees';
export const ORDERS = 'Orders';
export const PRODUCTS = 'Products';
export const PEOPLE = 'People';