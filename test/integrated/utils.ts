import { NORTWIND_V4_BASE, CUSTOMERS, PEOPLE, TRIP_PIN_BASE } from './constants';
import fetch from 'node-fetch';

export async function doFetch(url: string, options?: any): Promise<any> {
    const result = await fetch(url);
    return result.json();
}

export function buildNorthWindUrl(entitySet = CUSTOMERS, queryString?: string): string {
    return `${NORTWIND_V4_BASE}/${entitySet}${queryString ? `?${queryString}` : ''}`;
}

export function buildTripPinUrl(entitySet = PEOPLE, queryString?: string): string {
    return `${TRIP_PIN_BASE}/${entitySet}${queryString ? `?${queryString}` : ''}`
}
