import { OData4Filter } from '../../src/odata/odata4';
import { ODataFunctions } from '../../src/filter/ODataFunctions';

describe('OData 4 functions', () => {
	describe('Simple cases (uses eq everywhere)', () => {
		const cases = [
			{
				operator: 'toLower',
				args: [(f: ODataFunctions) => f.toLower('Name'), 'john'],
				expected: 'tolower(Name) eq \'john\''
			},
			{
				operator: 'concat',
				args: [(f: ODataFunctions) => f.concat('Name', 'Surname'), 'john'],
				expected: 'concat(Name,Surname) eq \'john\''
			},
			{
				operator: 'toUpper',
				args: [(f: ODataFunctions) => f.toUpper('Name'), 'JOHN'],
				expected: 'toupper(Name) eq \'JOHN\''
			},
			{
				operator: 'length',
				args: [(f: ODataFunctions) => f.length('Name'), 5],
				expected: 'length(Name) eq 5'
			},
			{
				operator: 'indexOf',
				args: [(f: ODataFunctions) => f.indexOf('Name'), 5],
				expected: 'indexof(Name) eq 5'
			},
			{
				operator: 'floor',
				args: [(f: ODataFunctions) => f.floor('Name'), 5],
				expected: 'floor(Name) eq 5'
			},
			{
				operator: 'ceiling',
				args: [(f: ODataFunctions) => f.ceiling('Name'), 5],
				expected: 'ceiling(Name) eq 5'
			},
			{
				operator: 'trim',
				args: [(f: ODataFunctions) => f.trim('Name'), 'John'],
				expected: 'trim(Name) eq \'John\''
			},
			{
				operator: 'round',
				args: [(f: ODataFunctions) => f.round('Name'), 5],
				expected: 'round(Name) eq 5'
			}
		];
		let filter: OData4Filter;

		beforeEach(() => {
			filter = new OData4Filter();
		});
		cases.forEach(test => {
			it(`should apply function ${test.operator} with args: ${test.args}`, () => {
				const filterString = filter.eq.apply(filter, test.args).toString();
				expect(filterString).toEqual(test.expected);
			});
		});
	});
	describe('Special cases', () => {
		let filter: OData4Filter;
		beforeEach(() => {
			filter = new OData4Filter();
		});

		it('should build nested concat', () => {
			const query = filter.eq(f => f.concat(x => x.concat('City', 'F'), 'X'), 'Berlin');
			expect(query.toString()).toEqual(`concat(concat(City,F),X) eq 'Berlin'`);
		});

		it('should build nested concat with differen canonical function', () => {
			const query = filter.eq(f => f.concat(x => x.toUpper('City'), 'X'), 'Berlin');
			expect(query.toString()).toEqual(`concat(toupper(City),X) eq 'Berlin'`);
		})
	})
})