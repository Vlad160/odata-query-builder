import { OData4Filter } from '../../src/odata/odata4';
import { LogicalOperator } from '../../src/filter/ODataFilter';
import { eq, gt } from '../../src/operators';

const cases = [
	{
		operator: 'contains',
		fn: 'Name',
		value: 'John',
		expected: 'contains(Name,\'John\')'
	},
	{
		operator: 'endswith',
		fn: 'Name',
		value: 'John',
		expected: 'endswith(Name,\'John\')'
	},
	{
		operator: 'eq',
		fn: 'Name',
		value: 'John',
		expected: 'Name eq \'John\''
	},
	{
		operator: 'gt',
		fn: 'Money',
		value: 2000,
		expected: 'Money gt 2000'
	},
	{
		operator: 'has',
		fn: 'Name',
		value: 'John',
		expected: 'Name has \'John\''
	},
	{
		operator: 'in',
		fn: 'Name',
		value: ['Milk', 'Cheese'],
		expected: 'Name in (\'Milk\',\'Cheese\')'
	},
	{
		operator: 'le',
		fn: 'Money',
		value: 2000,
		expected: 'Money le 2000'
	},
	{
		operator: 'lt',
		fn: 'Money',
		value: 2000,
		expected: 'Money lt 2000'
	},
	{
		operator: 'ne',
		fn: 'Name',
		value: 'John',
		expected: 'Name ne \'John\''
	},
	{
		operator: 'startswith',
		fn: 'Name',
		value: 'John',
		expected: 'startswith(Name,\'John\')'
	},
];

describe('ODataFilter', () => {

	let filter: OData4Filter;

	beforeEach(() => {
		filter = new OData4Filter()
	});

	it('should initialize', () => {
		const filter = new OData4Filter();
		expect(filter).toBeDefined();
	});

	cases.forEach(x => {
		it(`should apply operator ${x.operator} to ${x.fn} with value: ${x.value}`, () => {
			let q: string;
			if (Array.isArray(x.value)) {
				q = filter[x.operator](x.fn, ...x.value).toString();
			} else {
				q = filter[x.operator](x.fn, x.value).toString();
			}
			expect(q).toBe(x.expected);
		});
	});

	it('should apply complex filter', () => {
		const filterString = filter.eq('Name', 'John')
			.in('Product', 'Milk', 'Cheese')
			.or(f => f.gt('Price', 5)
				.endswith('Country', 'Germany'), LogicalOperator.OR)
			.toString();
		expect(filterString).toEqual('((Name eq \'John\') and (Product in (\'Milk\',\'Cheese\'))) or ((Price gt 5) or (endswith(Country,\'Germany\')))');
	});

	it('should apply piped operators', () => {
		const filterString = filter.pipe(eq('Name', 'John'), gt('Age', 5))
			.toString();
		expect(filterString).toEqual('(Name eq \'John\') and (Age gt 5)');
	});

	it('should apply and for lazy developer', () => {
		const filterString = filter.and(f => f.pipe(eq('Name', 'John')))
			.toString();
		expect(filterString).toEqual('Name eq \'John\'')
	});

	it('should apply add, not, or logical operators', () => {
		const filterString = filter.eq('Name', 'John')
			.gt('Age', 5)
			.or(f => f.eq('Name', 'Kate')
				.endswith('Age', 228), LogicalOperator.NOT)
			.toString();
		expect(filterString).toEqual('((Name eq \'John\') and (Age gt 5)) or ((Name eq \'Kate\') not (endswith(Age,228)))')
	});
});