import { OData4Query } from '../../src/odata/odata4';
import { expand, search, select, skip, top } from '../../src/operators';

export const simpleCases = [
	{
		operator: 'expand',
		args: ['Result'],
		expected: '$expand=Result'
	},
	{
		operator: 'expand',
		args: ['Result/Items'],
		expected: '$expand=Result($expand=Items)'
	},
	{
		operator: 'expand',
		args: [{ 'Result': { 'expand': { 'Items': {} } } }],
		expected: '$expand=Result($expand=Items)'
	},
	{
		operator: 'expand',
		args: [{ 'Result/Items': {} }],
		expected: '$expand=Result($expand=Items)'
	},
	{
		operator: 'expand',
		args: [{ 'Result': {}, 'Items': {} }],
		expected: '$expand=Result,Items'
	},
	{
		operator: 'expand',
		args: [['Result', 'Items']],
		expected: '$expand=Result,Items'
	},
	{
		operator: 'search',
		args: ['Search'],
		expected: '$search=Search'
	},
	{
		operator: 'select',
		args: ['Select'],
		expected: '$select=Select'
	},
	{
		operator: 'select',
		args: [['Select', 'Something']],
		expected: '$select=Select,Something'
	},
	{
		operator: 'skip',
		args: [10],
		expected: '$skip=10'
	},
	{
		operator: 'top',
		args: [10],
		expected: '$top=10'
	},
	{
		operator: 'count',
		expected: '$count=true'
	},
	{
		operator: 'raw',
		args: ['foo'],
		expected: 'foo'
	}
];

describe('Simple query test', () => {
	let query: OData4Query;
	beforeEach(() => {
		query = new OData4Query();
	});
	simpleCases.forEach(test => {
		it(`should ${test.operator} with args: ${JSON.stringify(test.args)}`, () => {
			const queryString = query[test.operator].apply(query, test.args).toString();
			expect(queryString).toBe(test.expected);
		})
	})
});

describe('Complex query test', () => {
	let query: OData4Query;
	beforeEach(() => {
		query = new OData4Query();
	});
	it('should top, skip, select, search, expand, orderBy', () => {
		const queryString = query.select('Select')
			.search('Search')
			.expand('Expand')
			.orderBy('OrderBy')
			.skip(5)
			.top(10)
			.toString();
		expect(queryString).toEqual('$expand=Expand&$select=Select&$orderBy=OrderBy&$top=10&$skip=5&$search=Search')
	});
	it('should nested query', () => {
		const queryString = query.expand('Result', q => q.select('Select')
			.expand('Items')).toString();
		expect(queryString).toEqual('$expand=Result($expand=Items;$select=Select)')
	});
	it('should pipe operator', () => {
		const queryString = query.pipe(select('Select'),
			search('Search'),
			expand('Expand'),
			skip(5),
			top(10))
			.toString();
		expect(queryString).toEqual('$expand=Expand&$select=Select&$top=10&$skip=5&$search=Search')
	});
	it('should make query with filter and top', () => {
		const queryString = query
			.filter(f => f.eq('Name', 'John'))
			.top(5)
			.toString();
		expect(queryString).toEqual('$top=5&$filter=Name eq \'John\'')
	});

	it('should make expand with levels', () => {
		const queryString = query
			.expand('People', q => q.levels(5))
			.toString()
		expect(queryString).toEqual('$expand=People($levels=5)');
	});
});