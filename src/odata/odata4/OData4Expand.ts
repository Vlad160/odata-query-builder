import { IQueryExpression, IExpressionable } from '../../core';
import { OData4Query } from './internal';

export interface IExpandExpression extends IQueryExpression {
    $levels: number
}

export class OData4Expand extends OData4Query implements IExpressionable<IExpandExpression> {
    private $levels: number;

    levels(level: number): this {
        this.$levels = level;
        return this;
    }

    toExpression(): IExpandExpression {
        const expression = super.toExpression();
        const $levels = this.$levels;
        return { ...expression, $levels };
    }

}