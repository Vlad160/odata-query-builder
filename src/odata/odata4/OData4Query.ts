import { ODataQuery } from '../../ODataQuery';
import { OData4Provider, OData4Expand } from './internal';
import { Expand } from '../../core/IExpand';

/**
 * OData 4 Query provider
 */
export class OData4Query extends ODataQuery {

    constructor() {
        super();
        this.provider = new OData4Provider();
    }

    /**
     * @inheritdoc
     */
    expand(expand: Expand, subquery?: (q: OData4Expand) => OData4Expand): this {
        if (subquery) {
            const queryBuilder = new OData4Expand();
            queryBuilder.parent = this;
            queryBuilder.provider = this.provider;
            const query = subquery(queryBuilder);
            this.buildExpand(expand, query);
            return this;
        }
        if (typeof expand === 'object' && Array.isArray(expand)) {
            (expand as Array<any>).forEach(x => this.expand(x));
        } else {
            this.buildExpand(expand);
        }
        return this;
    }

    protected buildExpand(expand: Expand, query?: any): void {
        if (typeof expand === 'string') {
            const expandQuery = new OData4Expand();
            expandQuery.parent = this;
            if (expand.indexOf('/') === -1) {
                this.$expand[expand] = ODataQuery.cast(query, this) || expandQuery;
            } else {
                const expanded = expand.indexOf('/');
                const first = expand.slice(0, expanded);
                expandQuery.expand({ [expand.slice(expanded + 1)]: query });
                this.$expand[first] = expandQuery;
            }
            return;
        } else if (typeof expand === 'object') {
            const keys = Object.keys(expand);
            keys.forEach(key => this.buildExpand(key, expand[key]));
            return;
        } else {
            // Shouldn't get there
            throw new Error(`Expand query of type ${typeof expand} is not suported or used in wrong format`);
        }

    }
}
