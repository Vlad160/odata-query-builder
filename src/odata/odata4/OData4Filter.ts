import { LogicalOperator, ODataFilter } from '../../filter/ODataFilter';
import { OData4Provider } from './internal';

export class OData4Filter extends ODataFilter {
    constructor(operator: LogicalOperator = LogicalOperator.AND) {
        super(operator);
        this.provider = new OData4Provider();
    }
}
