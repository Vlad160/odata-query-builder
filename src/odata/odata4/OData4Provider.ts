import { IExpression } from '../../filter/Expression';
import { ODataFilter } from '../../filter/ODataFilter';
import { OData4Expand, IExpandExpression } from './internal';
import { OData4Query } from './internal';

export class OData4Provider {
    filterToString(filter: ODataFilter, options: { encode: boolean } = { encode: false }): string {
        const expression = filter.toExpression();
        const { encode } = options;
        const encodeFn = encode ? encodeURIComponent : (str: string) => str;
        const { group, operator, children } = expression;
        let query = group.toExpression().rules.map((x: IExpression<any>) => x.toString());
        query = query.length > 1 ? query.map(x => `(${x})`) : query;
        let groupedQuery = query.join(` ${operator} `);
        // Handling empty query
        groupedQuery = children.length && query.length ? `(${groupedQuery})` : groupedQuery;
        const childrenQuery = children.map(x => `${x.operator} (${this.filterToString(x.child).trim()})`).join(' ');
        return encodeFn(`${groupedQuery} ${childrenQuery}`.trim());
    }

    toString(oDataQuery: OData4Query | OData4Expand, options: { encode: boolean } = { encode: false }): string {
        const query: Array<string> = [];
        const queryExpression = oDataQuery.toExpression();
        const sep = queryExpression.parent ? ';' : '&';
        const expandKeys = Object.keys(queryExpression.$expand);
        const { encode } = options;
        const encodeFn = encode ? encodeURIComponent : (str: string) => str;
        if (expandKeys.length) {
            const expand = expandKeys
                .reduce(
                    (acc, key: string) => {
                        const expandKey = queryExpression.$expand[key] as OData4Expand;
                        let expandQuery = this.toString(expandKey, options);
                        if (expandQuery) {
                            expandQuery = expandQuery ? `(${expandQuery})` : '';
                            acc.push(`${key}${expandQuery}`);
                        } else {
                            acc.push(key);
                        }
                        return acc;
                    },
                    [] as Array<string>,
                )
                .join(',');
            query.push(`$expand=${expand}`);
        }
        if (queryExpression.$select.length) {
            const select = `$select=${encodeFn(queryExpression.$select.join(','))}`;
            query.push(select);
        }
        if (queryExpression.$orderBy.length) {
            const orderBy = `$orderBy=${encodeFn(queryExpression.$orderBy.join(','))}`;
            query.push(orderBy);
        }
        if (queryExpression.$top || queryExpression.$top === 0) {
            query.push(`$top=${queryExpression.$top}`);
        }
        if (queryExpression.$skip || queryExpression.$skip === 0) {
            query.push(`$skip=${queryExpression.$skip}`);
        }
        if (queryExpression.$search || queryExpression.$search === '') {
            query.push(`$search=${encodeFn(queryExpression.$search)}`);
        }
        if (queryExpression.$filter) {
            const filterQuery = queryExpression.$filter;
            query.push(`$filter=${this.filterToString(filterQuery, options)}`);
        }
        if (queryExpression.raw) {
            query.push(queryExpression.raw);
        }
        if (queryExpression.$count) {
            query.push(`$count=true`);
        }
        if (isExpandQueryExpression(queryExpression)) {
            query.push(`$levels=${queryExpression.$levels}`);
        }
        return query.join(sep);
    }

}

function isExpandQueryExpression(expression: any): expression is IExpandExpression {
    return typeof expression.$levels !== 'undefined';
}
