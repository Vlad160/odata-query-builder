export interface IExpressionable<T> {
    toExpression(): T;
}
