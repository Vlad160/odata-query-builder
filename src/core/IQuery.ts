import { IBaseQuery } from './IBaseQuery';
import { IOrderedQuery, Order } from './IOrderedQuery';
import { ODataFilter } from '../filter/ODataFilter';
import { Expand } from './IExpand';

export interface IQuery extends IBaseQuery {
    select(select: Array<string> | string): this;

    search(search: string): this;

    orderBy(orderBy: string, order?: Order): IOrderedQuery;

    expand(expand: Expand, cb?: (q: IQuery) => IQuery): this;

    filter(f: (f: ODataFilter) => ODataFilter): this;

    raw(query: string): this;

    count(): this;
}

export interface IQueryExpression {
    $expand: Record<string, IQuery>;
    $skip: string | number;
    $top: string | number;
    $count: boolean;
    $search: string;
    $select: Array<string>;
    $orderBy: Array<string>;
    $filter: ODataFilter;
    parent: IQuery;
    raw: string;
}