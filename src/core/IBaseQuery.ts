export interface IBaseQuery {
    /**
     * @internal
     */
    parent: IBaseQuery;

    skip(skip: string | number): this;

    top(top: string | number): this;

    toString(): string;
}
