export interface IExpand {
    [key: string]: Partial<{
        select: string | Array<string>;
        search: string;
        expand: IExpand;
        skip: string | number;
        top: string | number;
    }>;
}

export type Expand = IExpand | string | Array<IExpand> | Array<string>;
