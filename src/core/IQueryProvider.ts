export interface IQueryProvider<T, X> {
    toString(query: T, options?: { encode: boolean }): string;

    filterToString(query: X, options?: { encode: boolean }): string;
}
