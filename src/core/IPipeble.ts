export interface IPipeble<T> {
    pipe(...args: Array<any>): T;
}
