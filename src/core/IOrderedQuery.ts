import { IBaseQuery } from './IBaseQuery';

export enum Order {
    ASC = 'asc',
    DESC = 'desc',
}

export interface IOrderedQuery extends IBaseQuery {
    thenBy(orderBy: string, order?: Order): this;
}
