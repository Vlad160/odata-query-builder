import { OrderedQuery } from './OrderedQuery';
import { ODataFilter } from './filter/ODataFilter';
import { IPipeble } from './core/IPipeble';
import { IQuery, IQueryExpression } from './core/IQuery';
import { Expand } from './core/IExpand';
import { IQueryProvider } from './core/IQueryProvider';
import { Order } from './core/IOrderedQuery';
import { IExpressionable } from './core';

export class ODataQuery implements IQuery, IPipeble<ODataQuery>, IExpressionable<IQueryExpression> {

    /**
     * @internal
     */
    parent: ODataQuery = null;
    /**
     * @internal
     */
    provider!: IQueryProvider<ODataQuery, ODataFilter>;
    protected $expand: Record<string, IQuery> = {};
    protected $skip!: string | number;
    protected $top!: string | number;
    protected $count!: boolean;
    protected $search!: string;
    protected $select: Array<string> = [];
    protected $orderBy: Array<string> = [];
    protected $filter!: ODataFilter;
    protected rawQuery!: string;

    /**
     * Cast plain query to ODataQuery
     * @param query
     * @param {ODataQuery} parent
     * @returns {ODataQuery}
     */
    static cast(query: any, parent: ODataQuery): ODataQuery {
        if (query instanceof ODataQuery) {
            return query;
        }
        if (!query || typeof query !== 'object') {
            return query;
        }
        const keys = Object.keys(query);
        const ctor = parent.constructor as typeof ODataQuery;
        const q: ODataQuery = new ctor();
        q.parent = parent;
        keys.forEach((key: string) => q[key](query[key]));
        return q;
    }

    /**
     * Sets top operator for current query
     * @param {string | number} top
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#sec_SystemQueryOptiontop
     * @returns {this}
     */
    top(top: string | number): this {
        this.$top = top;
        return this;
    }

    /**
     * Sets skip operator for current query
     * @param {string | number} skip
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#sec_SystemQueryOptionskip
     * @returns {this}
     */
    skip(skip: string | number): this {
        this.$skip = skip;
        return this;
    }

    /**
     * Sets count parameter for current query
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#sec_SystemQueryOptioncount
     * @returns {this}
     */
    count(): this {
        this.$count = true;
        return this;
    }

    /**
     * Applies multiple operators for current query
     * @param {Array<Function>} fns
     * @returns {this}
     */
    pipe(...fns: Array<(q: any) => ODataQuery>): this {
        fns.reduce((q: ODataQuery, fn) => fn(q), this);
        return this;
    }

    /**
     * Sets select for current query. Multiple parameters for multiple selects
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#_Toc505771200
     * @param {string} select
     * @returns {this}
     */
    select(select: string): this;
    select(...select: Array<string>): this;
    select(...select: Array<string>): this {
        this.$select = select;
        return this;
    }

    /**
     * Sets search string for current query. See docs for more
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#sec_SystemQueryOptionsearch
     * @param {string} search
     * @returns {this}
     */
    search(search: string): this {
        this.$search = search;
        return this;
    }

    /**
     * Sets filter for current query. See more in ODataFilter
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#sec_SystemQueryOptionfilter
     * @param {(f: ODataFilter) => ODataFilter} f
     * @returns {this}
     */
    filter(f: (f: ODataFilter) => ODataFilter): this {
        this.$filter = f(new ODataFilter());
        return this;
    }

    /**
     * Sets order for current query. Returns order query for continuous ordering
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#sec_SystemQueryOptionorderby
     * @param {string} orderBy
     * @param {Order} order
     * @returns {OrderedQuery}
     */
    orderBy(orderBy: string, order?: Order): OrderedQuery {
        this.$orderBy.push(`${orderBy} ${order || ''}`.trim());
        const orderedQuery = new OrderedQuery();
        orderedQuery.parent = this;
        return orderedQuery;
    }

    /**
     * Add expand node to current query
     * @link http://docs.oasis-open.org/odata/odata/v4.01/cs01/part1-protocol/odata-v4.01-cs01-part1-protocol.html#_Toc505771201
     * @param {IExpand | string | Array<IExpand> | Array<string>} expand
     * @param {(q: IQuery) => IQuery} subquery
     * @returns {this}
     */
    expand(expand: Expand, subquery?: (q: IQuery) => IQuery): this {
        if (subquery) {
            const queryBuilder = new ODataQuery();
            queryBuilder.parent = this;
            queryBuilder.provider = this.provider;
            const query = subquery(queryBuilder);
            this.buildExpand(expand, query);
            return this;
        }
        if (typeof expand === 'object' && Array.isArray(expand)) {
            (expand as Array<any>).forEach(x => this.expand(x));
        } else {
            this.buildExpand(expand);
        }
        return this;
    }

    toString(options: { encode: boolean } = { encode: false }): string {
        if (!this.provider) {
            throw new Error('Filter provider can not be null');
        }
        return this.provider.toString(this, options);
    }

    toExpression(): IQueryExpression {
        const { $expand, $filter, $orderBy, $search, $select, $skip, $top, $count, parent, rawQuery: raw } = this;
        return {
            $expand,
            $filter,
            $orderBy,
            $select,
            $search,
            $skip,
            $top,
            $count,
            parent,
            raw
        };
    }

    /**
     * Add raw query that will be added as is
     * @param query
     */
    raw(query: string): this {
        this.rawQuery = query;
        return this;
    }

    protected buildExpand(expand: Expand, query?: any): void {
        if (typeof expand === 'string') {
            const expandQuery = new ODataQuery();
            expandQuery.parent = this;
            if (expand.indexOf('/') === -1) {
                this.$expand[expand] = ODataQuery.cast(query, this) || expandQuery;
            } else {
                const expanded = expand.indexOf('/');
                const first = expand.slice(0, expanded);
                expandQuery.expand({ [expand.slice(expanded + 1)]: query });
                this.$expand[first] = expandQuery;
            }
            return;
        } else if (typeof expand === 'object') {
            const keys = Object.keys(expand);
            keys.forEach(key => this.buildExpand(key, expand[key]));
            return;
        } else {
            // Shouldn't get there
            throw new Error(`Expand query of type ${typeof expand} is not suported or used in wrong format`);
        }

    }
}
