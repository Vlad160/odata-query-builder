import { operatorFn, toExpression } from './core';

export function startswith(left: operatorFn, right: string | number) {
    return toExpression({
        value: 'startswith',
        args: [left, right],
        isFunction: true,
    });
}
