import { ODataFunctions } from '../../filter/ODataFunctions';
import { BinaryExpression, IExpression, UnaryExpression } from '../../filter/Expression';
import { IRule, Rule } from '../../filter/Rule';

export type operatorFn = ((f: ODataFunctions) => any) | string;

export type operatorFunc = (key: operatorFn, value: string | number | Array<any>) => (f: ODataFunctions) => Rule;

export function toRule(obj: any, f: ODataFunctions): IRule {
    if (!obj) {
        return obj;
    }
    if (typeof obj === 'function') {
        obj = obj(f);
    }
    if (typeof obj !== 'object' && typeof obj !== 'function') {
        obj = { value: obj };
    }
    if (obj.args) {
        obj.args = obj.args.map((x: any) => toRule(x, f));
    }
    return new Rule(obj);
}

export function toExpression(left: any, right?: any, operator?: string): (f: ODataFunctions) => IExpression<any> {
    return (f: ODataFunctions) => {
        const leftHandValue = new UnaryExpression(toRule(left, f));
        if (!right) {
            return leftHandValue;
        }
        operator = operator || '';
        const rightHandValue = new UnaryExpression(toRule(right, f));
        return new BinaryExpression({
            leftHandValue,
            rightHandValue,
            operator,
        });
    };
}
