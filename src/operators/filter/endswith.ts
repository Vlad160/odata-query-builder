import { operatorFn, toExpression } from './core';

export function endswith(left: operatorFn, right: string | number) {
    return toExpression({
        value: 'endswith',
        args: [left, right],
        isFunction: true,
    });
}
