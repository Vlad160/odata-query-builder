import { operatorFn, toExpression } from './core';

export function contains(left: operatorFn, right: string | number) {
    return toExpression({ value: 'contains', args: [left, right], isFunction: true });
}
