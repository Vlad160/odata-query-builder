import { operatorFn, toExpression } from './core';

export function ne(left: operatorFn, right: string | number) {
    return toExpression(left, right, 'ne');
}
