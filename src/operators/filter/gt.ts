import { operatorFn, toExpression } from './core';

export function gt(left: operatorFn, right: string | number) {
    return toExpression(left, right, 'gt');
}
