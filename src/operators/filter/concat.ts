import { operatorFn, toExpression } from './core';

export function concat(left: operatorFn, ...right: Array<string | number>) {
    return toExpression({ value: 'concat', args: [left, ...right], isFunction: true, normalize: false });
}
