import { operatorFn, toExpression } from './core';

export function lt(left: operatorFn, right: string | number) {
    return toExpression(left, right, 'lt');
}
