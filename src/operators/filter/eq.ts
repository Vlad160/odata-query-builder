import { operatorFn, toExpression } from './core';

export function eq(left: operatorFn, right: string | number) {
    return toExpression(left, right, 'eq');
}
