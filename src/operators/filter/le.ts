import { operatorFn, toExpression } from './core';

export function le(left: operatorFn, right: string | number) {
    return toExpression(left, right, 'le');
}
