import { operatorFn, toExpression } from './core';

export function In(left: operatorFn, ...right: Array<any>) {
    right[0] = `'${right[0]}'`;
    return toExpression(left, { args: right, isFunction: true }, 'in');
}
