import { operatorFn, toExpression } from './core';

export function has(left: operatorFn, right: string | number) {
    return toExpression(left, right, 'has');
}
