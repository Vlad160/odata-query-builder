import { IQuery } from '../../core/IQuery';

export function search<T extends IQuery>(search: string): (q: T) => T {
    return (q: T) => q.search(search);
}
