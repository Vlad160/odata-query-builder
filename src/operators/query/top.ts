import { IBaseQuery } from '../../core/IBaseQuery';

// tslint:disable-next-line
export function top<T extends IBaseQuery>(top: string | number): (q: T) => T {
    return (q: T) => q.top(top);
}
