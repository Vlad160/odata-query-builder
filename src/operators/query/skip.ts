import { IBaseQuery } from '../../core/IBaseQuery';

export function skip<T extends IBaseQuery>(skip: string | number): (q: T) => T {
    return (q: T) => q.skip(skip);
}
