import { IQuery } from '../../core/IQuery';

export function select<T extends IQuery>(select: string | Array<string>): (q: T) => T {
    return (q: T) => q.select(select);
}
