import { IQuery } from '../../core/IQuery';
import { Expand } from '../../core/IExpand';

export function expand<T extends IQuery>(expand: Expand, fn?: (q: IQuery) => IQuery): (q: T) => T {
    return (q: T) => q.expand(expand, fn);
}
