import { IExpressionable } from '../core/IExpressionable';
import { IRule, Rule } from './Rule';
import { quoteValue } from '../Utils';

export interface IExpression<T> extends IExpressionable<T> {
    toString(): string;
}

export interface IUnaryExpression extends IExpression<IUnaryExpressionIExpression> {
    value: IRule;
}

export interface IUnaryExpressionIExpression {
    value: Rule;
}

export class UnaryExpression implements IUnaryExpression {
    value: Rule;

    constructor(expression: IRule) {
        expression.args = (expression.args || []).map(x => Rule.cast(x));
        this.value = Rule.cast(expression);
    }

    toExpression(): IUnaryExpressionIExpression {
        return { value: this.value };
    }

    toString(): string {
        return this.value.toString();
    }
}

export interface IBinaryExpressionExpression {
    leftHandValue: IUnaryExpression;
    rightHandValue: IUnaryExpression;
    operator: string;
}

export interface IBinaryExpression extends IExpression<IBinaryExpressionExpression> {
    leftHandValue: IUnaryExpression;
    rightHandValue: IUnaryExpression;
    operator: string;
}

export class BinaryExpression implements IBinaryExpression {
    leftHandValue: IUnaryExpression;
    rightHandValue: IUnaryExpression;
    operator: string;

    constructor(expression: IBinaryExpressionExpression) {
        this.leftHandValue = expression.leftHandValue;
        this.rightHandValue = expression.rightHandValue;
        this.operator = expression.operator;
    }

    toExpression(): IBinaryExpressionExpression {
        const { leftHandValue, rightHandValue, operator } = this;
        return { leftHandValue, rightHandValue, operator };
    }

    toString(): string {
        const query = [];
        query.push(this.leftHandValue.toString());
        this.rightHandValue.value.value = quoteValue(this.rightHandValue.value.value);
        query.push(this.rightHandValue.toString());
        return query.join(` ${this.operator} `);
    }
}
