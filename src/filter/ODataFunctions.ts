import { Rule } from './Rule';
import { operatorFn } from '../operators/filter/core';

export enum ODataFunc {
    toLower = 'tolower',
    concat = 'concat',
    toUpper = 'toupper',
    length = 'length',
    indexOf = 'indexof',
    trim = 'trim',
    round = 'round',
    floor = 'floor',
    ceiling = 'ceiling'
}

export class ODataFunctions {
    toLower(str: string): Rule {
        return this.toFuncRule(ODataFunc.toLower, str);
    }

    concat(str: operatorFn, second: string): Rule {
        return this.toFuncRule(ODataFunc.concat, str, false, second);
    }

    toUpper(str: string): Rule {
        return this.toFuncRule(ODataFunc.toUpper, str);
    }

    length(str: string): Rule {
        return this.toFuncRule(ODataFunc.length, str);
    }

    indexOf(str: string): Rule {
        return this.toFuncRule(ODataFunc.indexOf, str);
    }

    floor(str: string): Rule {
        return this.toFuncRule(ODataFunc.floor, str);
    }

    ceiling(str: string): Rule {
        return this.toFuncRule(ODataFunc.ceiling, str);
    }

    trim(str: string): Rule {
        return this.toFuncRule(ODataFunc.trim, str);
    }

    round(str: string): Rule {
        return this.toFuncRule(ODataFunc.round, str);
    }

    private toFuncRule(func: ODataFunc, left: operatorFn, normalize = true, ...restArgs: Array<string | number>): Rule {
        const value = typeof left === 'function' ? left(this) : left;
        const args = restArgs.map(x => ({ value: x, isFunction: false }));
        return new Rule({ isFunction: true, args: [new Rule({ value, isFunction: false }), ...args], value: func, normalize });
    }
}
