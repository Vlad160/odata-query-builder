import { ILogicalGroup, LogicalOperator } from './ODataFilter';
import { Rule } from './Rule';
import { IExpressionable } from '../core/IExpressionable';

export interface IFilterGroupExpression {
    operator: LogicalOperator;
    rules: Array<Rule>;
}

export class FilterGroup implements ILogicalGroup, IExpressionable<IFilterGroupExpression> {
    operator: LogicalOperator = LogicalOperator.AND;
    rules: Array<Rule> = [];

    constructor(config: Partial<ILogicalGroup>) {
        this.operator = config.operator || this.operator;
        this.rules = config.rules || this.rules;
    }

    toExpression(): IFilterGroupExpression {
        return {
            operator: this.operator,
            rules: this.rules,
        };
    }
}
