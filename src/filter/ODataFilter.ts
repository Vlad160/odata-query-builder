import { ODataFunctions } from './ODataFunctions';
import { FilterGroup } from './FilterGroup';
import { IPipeble } from '../core/IPipeble';
import { AbstractODataFilter } from './AbstractODataFilter';
import { Rule } from './Rule';
import { IQueryProvider } from '../core/IQueryProvider';
import { IExpressionable } from '../core/IExpressionable';

export enum LogicalOperator {
    AND = 'and',
    OR = 'or',
    NOT = 'not',
}

export interface ILogicalGroup {
    operator: LogicalOperator;
    rules: Array<Rule>;
}

export interface IODataFilterChild {
    child: ODataFilter;
    operator: LogicalOperator;
}

export interface IODataFilterExpression {
    children: Array<IODataFilterChild>;
    group: FilterGroup;
    operator: LogicalOperator
}

export class ODataFilter extends AbstractODataFilter implements IPipeble<ODataFilter>, IExpressionable<IODataFilterExpression> {
    protected provider!: IQueryProvider<any, ODataFilter>;
    private children: Array<IODataFilterChild> = [];

    constructor(private operator = LogicalOperator.AND) {
        super();
        this.group = new FilterGroup({ operator });
    }

    static merge(tree: ODataFilter, subtree: ODataFilter): ODataFilter {
        if (tree.operator !== subtree.operator) {
            throw new Error('Can not merge filters with different logical operators');
        }
        tree.children = tree.children.concat(subtree.children);
        tree.group.rules = tree.group.rules.concat(subtree.group.rules);
        return tree;
    }

    pipe(...args: Array<any>): this {
        const rules = (args || []).map(x => x(new ODataFunctions()));
        this.group.rules.push(...rules);
        return this;
    }

    and(fn: (f: ODataFilter) => ODataFilter, continueWith: LogicalOperator = LogicalOperator.AND): this {
        this.addFilterNode(fn, LogicalOperator.AND, continueWith);
        return this;
    }

    or(fn: (f: ODataFilter) => ODataFilter, continueWith: LogicalOperator = LogicalOperator.AND): this {
        this.addFilterNode(fn, LogicalOperator.OR, continueWith);
        return this;
    }

    not(fn: (f: ODataFilter) => ODataFilter, continueWith: LogicalOperator = LogicalOperator.AND): this {
        this.addFilterNode(fn, LogicalOperator.NOT, continueWith);
        return this;
    }

    toExpression(): IODataFilterExpression {
        return {
            children: this.children,
            group: this.group,
            operator: this.operator,
        };
    }

    toString(options: { encode: boolean } = { encode: false }): string {
        if (!this.provider) {
            throw new Error('Filter provider can not be null');
        }
        return this.provider.filterToString(this, options);
    }

    private addFilterNode(
        fn: (f: ODataFilter) => ODataFilter,
        logicalOperator: LogicalOperator,
        continueWith: LogicalOperator,
    ): void {
        this.operator !== logicalOperator
            ? this.children.push({
                child: fn(new ODataFilter(continueWith)),
                operator: logicalOperator,
            })
            : ODataFilter.merge(this, fn(new ODataFilter()));
    }
}
