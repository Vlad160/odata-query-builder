import { FilterGroup } from './FilterGroup';
import * as op from './../operators/filter';
import { ODataFunctions } from './ODataFunctions';
import { operatorFn, operatorFunc } from '../operators/filter/core';
import { Rule } from './Rule';

export enum ODataOperators {
    eq = 'eq',
    ne = 'ne',
    gt = 'gt',
    ge = 'ge',
    lt = 'lt',
    le = 'le',
    has = 'has',
    in = 'In',
    contains = 'contains',
    endswith = 'endswith',
    startswith = 'startswith'
}

export abstract class AbstractODataFilter {
    protected group!: FilterGroup;

    eq(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.eq, expression, value));
        return this;
    }

    ne(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.ne, expression, value));
        return this;
    }

    gt(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.gt, expression, value));
        return this;
    }

    ge(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.ge, expression, value));
        return this;
    }

    lt(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.lt, expression, value));
        return this;
    }

    le(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.le, expression, value));
        return this;
    }

    has(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.has, expression, value));
        return this;
    }

    in(expression: operatorFn, ...value: Array<string>): this {
        this.group.rules.push(this.toRule(ODataOperators.in, expression, value));
        return this;
    }

    contains(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.contains, expression, value));
        return this;
    }

    endswith(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.endswith, expression, value));
        return this;
    }

    startswith(expression: operatorFn, value: string | number): this {
        this.group.rules.push(this.toRule(ODataOperators.startswith, expression, value));
        return this;
    }

    private toRule(operator: ODataOperators, left: operatorFn, right: any): Rule {
        // Get function
        const operatorFunction: operatorFunc = (op as any)[operator];
        // Invoke operator function with arguments
        // Result = callback
        const cbRule: (...args: Array<any>) => Rule = Array.isArray(right)
            ? operatorFunction.call(null, left, ...right)
            : operatorFunction.call(null, left, right);
        // Invoke callback with operators
        return cbRule(new ODataFunctions());
    }
}
