import { IExpressionable } from '../core/IExpressionable';
import { quoteValue } from '../Utils';

export interface IRule {
    value: string | number;
    isFunction: boolean;
    args?: Array<IRule>;
    normalize?: boolean | ((str: string) => boolean);
    toString(): string;
}

export class Rule implements IRule, IExpressionable<IRule> {
    args: Array<IRule>;
    isFunction: boolean;
    value: string | number;
    normalize: boolean | ((str: string) => boolean);

    constructor(rule: IRule) {
        this.value = rule.value;
        this.isFunction = rule.isFunction;
        this.args = (rule.args || []).map((x: IRule) => Rule.cast(x));
        this.normalize = typeof rule.normalize === 'undefined' ? true : rule.normalize;
    }

    static cast(rule: IRule): Rule {
        if (rule instanceof Rule) {
            return rule;
        }
        return new Rule(rule);
    }

    toExpression(): IRule {
        const { value, args, isFunction } = this;
        return { value, args, isFunction };
    }

    toString(): string {
        if (!this.isFunction) {
            return this.value.toString();
        }
        this.args = this.args || [];
        let args: Array<any>;
        if (this.normalize && this.args.length > 1) {
            args = this.args.map((x, i) => {
                if (i > 0) {
                    x.value = quoteValue(x.value);
                }
                return x;
            });
        }
        args = this.args.map(x => x.toString());
        return `${this.value || ''}(${args.join(',')})`;
    }
}
