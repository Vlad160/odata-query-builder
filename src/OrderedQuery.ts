import { IBaseQuery } from './core/IBaseQuery';
import { IQuery } from './core/IQuery';
import { Order } from './core/IOrderedQuery';

export class OrderedQuery implements IBaseQuery {
    /**
     * @internal
     */
    parent: IQuery;

    thenBy(orderBy: string, order?: Order): this {
        this.parent.orderBy(orderBy, order);
        return this;
    }

    top(top: string | number): this {
        this.parent.top(top);
        return this;
    }

    skip(skip: string | number): this {
        this.parent.skip(skip);
        return this;
    }

    toString(): string {
        return this.parent.toString();
    }
}
