export const quoteValue = (x: any) => (typeof x === 'string' ? `'${x}'` : x);
